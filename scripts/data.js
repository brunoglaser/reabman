// Define the `reabManApp` module
var reabManApp = angular.module('reabManApp', ['firebase']);

// Initialize Firebase
var config = {
    apiKey: "AIzaSyB8Q14UoJ68Y-QZbTjMbK38D3JCPuvIyAc",
    authDomain: "reabman.firebaseapp.com",
    databaseURL: "https://reabman.firebaseio.com",
    storageBucket: "reabman.appspot.com",
    messagingSenderId: "77510856126"
};
firebase.initializeApp(config);

// Define the `PatientListController` controller on the `reabManApp` module
reabManApp.controller('PatientListController', function PatientListController($scope, $firebaseArray) {

    var ref = firebase.database().ref().child("patients");
    // download the data into a local object
    $scope.patients = $firebaseArray(ref);

    // create current patient object
    $scope.currPct = {}

    // Define patient selection behaviour
    $scope.clearCurrPct = function() {
        $scope.currPct.id = null;
        $scope.currPct.name = null;
        $scope.currPct.lastName = null;
        $scope.currPct.responsible = null;
        $scope.currPct.address = null;
        $scope.currPct.phone = null;
        $scope.currPct.email = null;
        $scope.currPct.firstAppointment = null;
        $scope.currPct.lastAppointment = null;
    }

    $scope.selectPatient = function(id) {
        if (angular.isUndefined(id) || id == null) {
            $scope.clearCurrPct();
        } else {
            $scope.currPct.id = $scope.patients[id].id;
            $scope.currPct.name = $scope.patients[id].name;
            $scope.currPct.lastName = $scope.patients[id].lastName;
            $scope.currPct.responsible = $scope.patients[id].responsible;
            $scope.currPct.address = $scope.patients[id].address;
            $scope.currPct.phone = $scope.patients[id].phone;
            $scope.currPct.email = $scope.patients[id].email;
            $scope.currPct.firstAppointment = $scope.patients[id].firstAppointment;
            $scope.currPct.lastAppointment = $scope.patients[id].lastAppointment;
        }
    }
});
